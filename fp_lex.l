%{
/*
 * $Id: fp_lex.l,v 2.9 1999/12/05 02:50:13 jcp Exp $
 *
 * Copyright (C) 1998, John C. Peterson <mailto:jaypee@netcom.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,
 * version 2. A copy is included in this distribution in the file
 * named "LICENSE".
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License, version 2, for more details.
 *
 * Note that the version 1.3 release of fplan (and older releases)
 * are the work of Steve Tynor <mailto:tynor@atlanta.twr.com>. Those
 * versions of fplan are in the public domain, (provided the source
 * code headers are left intact). Here is the original header;
 * 
 */

/*
 *----------------------------------------------------------------------------
 *	FPLAN - Flight Planner
 *	Steve Tynor
 *	tynor@prism.gatech.edu
 *
 *	This program is in the public domain. Permission to copy,
 * distribute, modify this program is hearby given as long as this header
 * remains. If you redistribute this program after modifying it, please
 * document your changes so that I do not take the blame (or credit) for
 * those changes.  If you fix bugs or add features, please send me a
 * patch so that I can keep the 'official' version up-to-date.
 *
 *	Bug reports are welcome and I'll make an attempt to fix those
 * that are reported.
 *
 *	USE AT YOUR OWN RISK! I assume no responsibility for any
 * errors in this program, its database or documentation. I will make an
 * effort to fix bugs, but if you crash and burn because, for example,
 * fuel estimates in this program were inaccurate, it's your own fault
 * for trusting somebody else's code! Remember, as PIC, it's _your_
 * responsibility to do complete preflight planning. Use this program as
 * a flight planning aid, but verify its results before using them.
 *----------------------------------------------------------------------------
 */

/*
 * This scanner requires Vern Paxson's "flex" lexical analyzer generator
 */

static char rcsid[] = "$Id: fp_lex.l,v 2.9 1999/12/05 02:50:13 jcp Exp $";

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "common.h"
#include "fp_tok.h"

/* define typedefs */

typedef struct {
   YY_BUFFER_STATE buffer;
   char *filename;
   int lineno;
} INC_STACK_T;

/* declare static variables */

static INC_STACK_T inc_stack[MAX_INCLUDE_DEPTH];
static int inc_stack_ptr = 0;

/* declare global variables */

char *filename_in;
int lineno_in = 1;
double yydval;

/* error handler function needed by yyparse () */

int 
yyerror (char *s)
{
   int i;
   /* print filename associated with current input stream */
   fprintf (stderr, "ERROR in file: %s, line no: %d\n", filename_in, lineno_in);
   /* print all include files and line numbers on the stack */
   if (inc_stack_ptr > 0) {
      for (i = inc_stack_ptr - 1; i >= 0; i--) {
	 fprintf (stderr, "\tincluded from file: %s, line no: %d\n",
	    inc_stack[i].filename, inc_stack[i].lineno);
      }
   }
   /* print the error string */
   fprintf (stderr, "%s\n", s);
   exit (EXIT_BAD);
}

/* end of file handler function needed by yylex () */

int 
yywrap (void)
{
   return (1);
}

%}

%x INCLUDE_BRACKET
%x INCLUDE_QUOTE

%%

\#		{ char c;
		  do {
		     c = input();
		  } while (c != '\n');
		  lineno_in++;
		};

from		return TOK_FROM;
via		return TOK_VIA;
to		return TOK_TO;

alt		return TOK_ALT;
comment		return TOK_COMMENT;
fuel_amount	return TOK_FUEL_AMOUNT;
fuel_rate	return TOK_FUEL_RATE;
fuel_used	return TOK_FUEL_USED;
nav		return TOK_NAV;
tas		return TOK_TAS;
wind		return TOK_WIND;

mi		return TOK_SMI;
smi		return TOK_SMI;
mph		return TOK_MPH;
nm		return TOK_NMI;
nmi		return TOK_NMI;
knots		return TOK_KTS;
kts		return TOK_KTS;

north		return TOK_NORTH;
south		return TOK_SOUTH;
east		return TOK_EAST;
west		return TOK_WEST;

\@		return TOK_ATSIGN;
\:		return TOK_COLON;
\/		return TOK_FSLASH;
\(		return TOK_LPAREN;
\)		return TOK_RPAREN;
\;		return TOK_SEMICOLON;

include[ \t]*\<[ \t]*	{ BEGIN (INCLUDE_BRACKET); };

include[ \t]*\"[ \t]*	{ BEGIN (INCLUDE_QUOTE); };

[\+\-]?[0-9]+\.[0-9]+	{ yydval = strtod (yytext, (char **) NULL);
			  /* error checking not required (lex did it) */
			  return TOK_REAL;
			};

[\+\-]?[0-9]+   	{ yydval = strtod (yytext, (char **) NULL);
			  /* error checking not required (lex did it) */
			  return TOK_INTEGER;
			};

\"		{ int i;
		  for (i = 0; (yytext[i] = input()) != '\"'; i++) {
		     if (yytext[i] == '\"')
			break;
		     if (yytext[i] == '\\')
			yytext[i] = input();
		  }
		  yytext[i] = '\0';
		  return TOK_STRING;
		};

\_?[0-9A-Za-z]+ return TOK_IDENT; /* allow leading _ for personal waypoints */

[ ,\t]       	;	/* ignore whitespace characters */

\n		{ lineno_in++; };

.		{
		  /* if no match to above rules, it's a syntax error */
		  yyerror ("syntax error: unrecognized token");
		};

<INCLUDE_BRACKET>[^\>]*\>[ \t]*\;	{

		  char buffer[128];
		  char filename[MAXPATHLEN];
		  char *common_dir;
		  char *user_dir;

		  /* locate the closing '>' character, null terminate string */

		  char *p = strchr(yytext, '>');

		  p[0] = '\0';

		  /* check depth of the include stack */

		  if (inc_stack_ptr < MAX_INCLUDE_DEPTH) {
		     /* save the current state on the stack */
		     inc_stack[inc_stack_ptr].buffer = YY_CURRENT_BUFFER;
		     inc_stack[inc_stack_ptr].filename = filename_in;
		     inc_stack[inc_stack_ptr].lineno = lineno_in;
		     inc_stack_ptr++;
		  } else {
		     yyerror ("recursive includes are too deeply nested");
		  }

		  /* locate and open the include file */

		  if ((yytext[0] == '~') ||
		     (yytext[0] == DIR_SEPARATOR_CHAR) ||
		     (!strcmp(yytext, CURRENT_DIRECTORY)) ||
		     (!strcmp(yytext, PARENT_DIRECTORY))) {
		     /* filename contains an absolute or relative path */
		     EXPAND_TILDE (filename, yytext);
		     yyin = fopen (filename, "r");
		  } else {
		     /* search the common fplan data directory first */
		     if (! (common_dir = getenv (FPLAN_COMMON_DBDIR)))
			common_dir = DEFAULT_COMMON_DBDIR;
		     strcpy (filename, common_dir);
		     strcat (filename, DIR_SEPARATOR_STR);
		     strcat (filename, yytext);
		     if (! (yyin = fopen (filename, "r"))) {
			/* search the user's fplan data directory next */
			if (! (common_dir = getenv (FPLAN_USER_DBDIR)))
			   user_dir = DEFAULT_USER_DBDIR;
			EXPAND_TILDE (filename, user_dir);
			strcat (filename, DIR_SEPARATOR_STR);
			strcat (filename, yytext);
			if (! (yyin = fopen (filename, "r"))) {
			   /* search current working directory last */
			   yyin = fopen (yytext, "r");
			}
		     }
		  }

		  if (! yyin) {
		     sprintf (buffer,
			"include file does not exist or is unreadable: %s",
			yytext);
		     yyerror (buffer);
		  }

		  /* reset lineno and filename, create new input buffer */

		  lineno_in = 1;
		  filename_in = strdup (yytext); /* next line resets yytext! */
		  yy_switch_to_buffer (yy_create_buffer (yyin, YY_BUF_SIZE));

		  /* scan the new input buffer */

		  BEGIN (INITIAL);

		};

<INCLUDE_BRACKET>.*	{
		  /* include syntax error */
		  yyerror ("syntax error");
		};

<INCLUDE_QUOTE>[^\"]*\"[ \t]*\;		{

		  char buffer[128];
		  char filename[MAXPATHLEN];
		  char *common_dir;
		  char *user_dir;

		  /* locate the closing '"' character, null terminate string */

		  char *p = strchr(yytext, '"');

		  p[0] = '\0';

		  /* check depth of the include stack */

		  if (inc_stack_ptr < MAX_INCLUDE_DEPTH) {
		     /* save the current state on the stack */
		     inc_stack[inc_stack_ptr].buffer = YY_CURRENT_BUFFER;
		     inc_stack[inc_stack_ptr].filename = filename_in;
		     inc_stack[inc_stack_ptr].lineno = lineno_in;
		     inc_stack_ptr++;
		  } else {
		     yyerror ("recursive includes are too deeply nested");
		  }

		  /* locate and open the include file */

		  if ((yytext[0] == '~') ||
		     (yytext[0] == DIR_SEPARATOR_CHAR) ||
		     (!strcmp(yytext, CURRENT_DIRECTORY)) ||
		     (!strcmp(yytext, PARENT_DIRECTORY))) {
		     /* filename contains an absolute or relative path */
		     EXPAND_TILDE (filename, yytext);
		     yyin = fopen (filename, "r");
		  } else {
		     /* search the user's fplan data directory first */
		     if (! (user_dir = getenv (FPLAN_USER_DBDIR)))
			user_dir = DEFAULT_USER_DBDIR;
		     EXPAND_TILDE (filename, user_dir);
		     strcat (filename, DIR_SEPARATOR_STR);
		     strcat (filename, yytext);
		     if (! (yyin = fopen (filename, "r"))) {
			/* search the common fplan data directory next */
			if (! (common_dir = getenv (FPLAN_COMMON_DBDIR)))
			   common_dir = DEFAULT_COMMON_DBDIR;
			strcpy (filename, common_dir);
			strcat (filename, DIR_SEPARATOR_STR);
			strcat (filename, yytext);
			if (! (yyin = fopen (filename, "r"))) {
			   /* search current working directory last */
			   yyin = fopen (yytext, "r");
			}
		     }
		  }

		  if (! yyin) {
		     sprintf (buffer,
			"include file does not exist or is unreadable: %s",
			yytext);
		     yyerror (buffer);
		  }

		  /* reset lineno and filename, create new input buffer */

		  lineno_in = 1;
		  filename_in = strdup (yytext); /* next line resets yytext! */
		  yy_switch_to_buffer (yy_create_buffer (yyin, YY_BUF_SIZE));

		  /* scan the new input buffer */

		  BEGIN (INITIAL);

		};

<INCLUDE_QUOTE>.*	{
		  /* include syntax error */
		  yyerror ("syntax error");
		};

<<EOF>>		{
		  if (inc_stack_ptr > 0) {
		     /* free the current input buffer */
		     yy_delete_buffer (YY_CURRENT_BUFFER);
		     /* restore the previous state from the stack */
		     yy_switch_to_buffer (inc_stack[--inc_stack_ptr].buffer);
		     filename_in = inc_stack[inc_stack_ptr].filename;
		     lineno_in = inc_stack[inc_stack_ptr].lineno;
		  } else {
		     yyterminate ();
		  }
		};
%%
