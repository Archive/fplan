#!/bin/sh
#
# build_bin_dist.sh - bourne shell script to build a new binary distribution
#
# $Id$
#

DIST_DIR="/home/jcp/dat/html/fplan"
SRC_DIR="/home/jcp/src/fplan"
RELEASE_STR="1.4.2"
GID_OWNER="users"
UID_OWNER="jcp"

#
# location of rpm build directory
#

if [ "$USER" = "root" ];then
  RPM_TOP="/usr/src/redhat"
else
  RPM_TOP="${HOME}/src/redhat"
fi

#
# construct file names
#

umask 022
ALL_BASENAME="fplan-${RELEASE_STR}"
BIN_TAR_BASENAME="${ALL_BASENAME}.i386.tar"
BIN_TAR_BZ2="${BIN_TAR_BASENAME}.bz2"
BIN_TAR_GZ="${BIN_TAR_BASENAME}.gz"
BIN_TAR_Z="${BIN_TAR_BASENAME}.Z"
BIN_TAR="${BIN_TAR_BZ2} ${BIN_TAR_GZ} ${BIN_TAR_Z}"
SRC_TAR_BASENAME="${ALL_BASENAME}.src.tar"
SRC_TAR_GZ="${SRC_TAR_BASENAME}.gz"

#
# check that the source tarball is available
#

echo "Ready to build binary distribution files for release: ${RELEASE_STR} ..."
echo -n "Have you built the source distribution files? (y/n): "
read ans

if [ "${ans}" != "y" ];then
   echo "Build the source distribution and re-run this script" 1>&2
   exit 1
fi

if [ ! -r ${DIST_DIR}/${SRC_TAR_GZ} ];then
   echo "Hey fruity! The source tarball is not there!" 1>&2
   exit 1
fi

#
# build the RPM source and binary archives
#

# get source tarball
cd ${RPM_TOP}
cp -p ${DIST_DIR}/${SRC_TAR_GZ} SOURCES/

# unpack the spec file
cd BUILD/
tar xvzf ../SOURCES/${SRC_TAR_GZ} ${ALL_BASENAME}/fplan.spec
mv ${ALL_BASENAME}/fplan.spec ../SPECS

# build source and binary rpm archives
cd ../SPECS
if [ "$USER" = "root" ];then
  chown root.root ../SOURCES/${SRC_TAR_GZ} fplan.spec
  chmod 644 ../SOURCES/${SRC_TAR_GZ} fplan.spec
fi
rpm -ba fplan.spec
rm -rf ../BUILD/${ALL_BASENAME}/

# copy the source and binary rpm archives
BIN_RPM=`cd ../RPMS/i386/ ; echo ${ALL_BASENAME}*.i386.rpm`
SRC_RPM=`cd ../SRPMS/ ; echo ${ALL_BASENAME}*.src.rpm`
mv ../RPMS/i386/${BIN_RPM} ../SRPMS/${SRC_RPM} ${DIST_DIR}/

# set file owner, prmissions
cd ${DIST_DIR}/
chown ${UID_OWNER}.${GID_OWNER} ${BIN_RPM} ${SRC_RPM}
chmod 644 ${BIN_RPM} ${SRC_RPM}

#
# build the Linux i386 tarballs
#

# extract binaries from rpm binary archive
rm -rf /tmp/${ALL_BASENAME}
mkdir /tmp/${ALL_BASENAME}
cd /tmp/${ALL_BASENAME}
rpm2cpio ${DIST_DIR}/${BIN_RPM} | cpio -idv --preserve-modification-time
# dont know why, but new dir permissions are all hosed up?
find . -type d -exec chmod 755 {} \;

# include the INSTALL.bin file for the tarball distribution
cd ./usr
cp -p ${SRC_DIR}/INSTALL.bin ./
if [ "$USER" = "root" ];then
  chown root.root INSTALL.bin
fi
chmod 644 INSTALL.bin
cp -p INSTALL.bin doc/${ALL_BASENAME}/

# create and compress the tar file
tar cvf ${BIN_TAR_BASENAME} *
bzip2 -kv9 ${BIN_TAR_BASENAME}
compress -c ${BIN_TAR_BASENAME} > ${BIN_TAR_Z}
gzip -cNv9 ${BIN_TAR_BASENAME} > ${BIN_TAR_GZ}
touch -r ${BIN_TAR_BASENAME} ${BIN_TAR}
chown ${UID_OWNER}.${GID_OWNER} ${BIN_TAR}
chmod 644 ${BIN_TAR}
mv ${BIN_TAR} ${DIST_DIR}/

# clean up
cd /tmp/
rm -rf /tmp/${ALL_BASENAME}

#
# all done
#

