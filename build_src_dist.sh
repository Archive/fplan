#!/bin/sh
#
# build_src_dist.sh - bourne shell script to build a new source distribution
#
# $Id$
#

DIST_DIR="/home/jcp/dat/html/fplan"
SRC_DIR="/home/jcp/src/fplan"
RELEASE_STR="1.4.2"

#
# construct file names
#

umask 022
ALL_BASENAME="fplan-${RELEASE_STR}"
SRC_TAR_BASENAME="${ALL_BASENAME}.src.tar"
SRC_TAR_BZ2="${SRC_TAR_BASENAME}.bz2"
SRC_TAR_GZ="${SRC_TAR_BASENAME}.gz"
SRC_TAR_Z="${SRC_TAR_BASENAME}.Z"
SRC_TAR="${SRC_TAR_BZ2} ${SRC_TAR_GZ} ${SRC_TAR_Z}"
ZIP_LIB="${ALL_BASENAME}.zip"

#
# check that all the changes have been commited to the CVS archive
#

echo "Ready to build source distribution files for release: ${RELEASE_STR} ..."
echo -n "Have you commited your changes to the CVS archive? (y/n): "
read ans

if [ "${ans}" != "y" ];then
   echo "Commit your changes, then re-run this script" 1>&2
   exit 1
fi

#
# build the UNIX source tarballs
#

cd $SRC_DIR
make veryclean
make doc
make dist
make veryclean
chmod 644 ${SRC_TAR}
mv ${SRC_TAR} ${DIST_DIR}/
rm -f ${ZIP_LIB}

#
# build the OS/2 and MS-DOS info zip source archive
#

cd $SRC_DIR
MAKEFLAGS="-f Makefile.EMX"
make ${MAKEFLAGS} veryclean
make ${MAKEFLAGS} doc
make ${MAKEFLAGS} dist
make ${MAKEFLAGS} veryclean
chmod 644 ${ZIP_LIB}
mv ${ZIP_LIB} ${DIST_DIR}/
rm -f ${SRC_TAR}

#
# all done
#

